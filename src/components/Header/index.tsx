import * as React from 'react';
import { Logo } from '../ui';
import { useState } from 'react';
import { MenuContainer, MenuItemsContainer, MenuBurger, InfoBox, MenuItem } from './styled';

export default function Header() {
    const documentWidth = document.documentElement.clientWidth;
    const windowWidth = window.innerWidth;
    const scrollBarWidth = windowWidth - documentWidth;

    const [isMenuOpen, switchMenu] = useState<boolean>(false);
    const [isMenuItemActive, activateMenuItem] = useState<string>();

    if (isMenuOpen) {
        console.log(isMenuOpen, scrollBarWidth);
        document.body.style.paddingRight = `${scrollBarWidth}px;`;
        document.body.style.overflow = 'hidden';
    } else {
        document.body.style.overflow = 'auto';
        document.body.style.paddingRight = '0px';
    }

    return (
        <MenuContainer>
            <a href="/#" onClick={() => activateMenuItem('')}>
                <Logo isWhite={isMenuOpen} />
            </a>
            <MenuItemsContainer isMenuOpen={isMenuOpen}>
                {MENU_ITEMS.map((item: MenuItemType, ind: number) => {
                    return (
                        <MenuItem
                            active={isMenuItemActive === item.href}
                            key={ind}
                            onClick={() => activateMenuItem(item.href)}
                        >
                            <a href={item.href} onClick={() => switchMenu(false)}>
                                {item.title}
                            </a>
                        </MenuItem>
                    );
                })}
                <InfoBox>
                    <span className="email">admin@blueonlinesolutions.com</span>
                    <p className="phone">+972 52 8515949</p>
                </InfoBox>
            </MenuItemsContainer>
            <MenuBurger isMenuOpen={isMenuOpen} onClick={() => switchMenu(!isMenuOpen)} />
        </MenuContainer>
    );
}

type MenuItemType = { title: string; href: string };

const MENU_ITEMS: MenuItemType[] = [
    { title: 'Our services', href: '#our-services' },
    { title: 'Cases', href: '#cases' },
    { title: 'About', href: '#about' },
    { title: 'Contact Us', href: '#contact-us' },
];
