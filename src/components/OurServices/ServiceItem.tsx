import React from 'react';
import { ServiceType } from './data';
import { ServiceBox, ServiceIcon } from './styled';

interface Props extends ServiceType {}

export const ServiceItem = (props: Props) => {
    return (
        <ServiceBox>
            <ServiceIcon name={props.icon} />
            <h3>{props.title}</h3>
            <p>{props.description}</p>
        </ServiceBox>
    );
};
