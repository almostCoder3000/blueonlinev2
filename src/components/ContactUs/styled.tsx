import styled from 'styled-components';
import { InputWrapper, Textarea } from '../TextInputForm/styled';
import { YellowButton } from '../ui';

export const PhoneImage = styled.div`
    display: none;
    @media only screen and (min-width: 1024px) {
        display: block;
        position: absolute;
        right: 30px;
        width: 25%;
        height: 50%;
        top: 130px;

        &::before {
            content: '';
            display: block;
            width: 100%;
            height: 100%;
            border-radius: 50%;
            background-color: #072847;
            opacity: 0.1;
        }

        &::after {
            content: '';
            display: block;
            width: 230px;
            height: 286px;
            background: url(${require('../../assets/imgs/contact-us-form-image.svg')}) 0 0 no-repeat;
            background-size: contain;
            position: absolute;
            top: 40px;
            right: 50px;
        }
    }
    @media only screen and (min-width: 1200px) {
        width: 407px;
        height: 407px;
        top: 60px;

        &::after {
            width: 360px;
            height: 386px;
            top: -20px;
            right: 80px;
        }
    }
    @media only screen and (min-width: 1440px) {
        right: calc(50% - 620px);
    }
`;

export const ContactUsContainer = styled.div`
    padding: 40px 25px 50px;
    h2 {
        margin: 0 0 30px;
    }

    ${YellowButton} {
        width: 100%;
        max-width: initial;
        input[type='submit'] {
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0);
            font-weight: 600;
            font-size: 18px;
            line-height: 110%;
            color: #072847;
            border: 0;
            cursor: pointer;
        }
    }

    @media only screen and (min-width: 768px) {
        padding: 40px 45px 50px;
        form {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            flex-wrap: wrap;
            margin: 0 -15px;

            & > div {
                padding: 0 15px;
            }
            & > label {
                margin-left: 15px;
                margin-top: 15px;
                & > span {
                    max-width: 350px;
                }
            }
            ${YellowButton} {
                width: 242px;
                margin-right: 15px;
            }
            ${InputWrapper}:not(:last-of-type) {
                width: 33.33%;
            }
            ${Textarea} {
                height: 100px;
                margin-bottom: 30px;
            }
        }
    }

    @media only screen and (min-width: 1024px) {
        margin-top: 120px;

        form {
            width: 75%;
        }
    }
    @media only screen and (min-width: 1200px) {
        form {
            width: 660px;
        }
    }
    @media only screen and (min-width: 1440px) {
        padding-left: calc(50% - 520px);
        padding-right: calc(50% - 520px);
    }
`;
