import styled, { createGlobalStyle } from 'styled-components';
import LogoIcon from '../../assets/imgs/logo.svg';

export const GlobalStyle = createGlobalStyle`
    body {
        margin: 0;
    }
    html {
        scroll-behavior: smooth;
    }
    * {
        box-sizing: border-box;
        color: #2E2E2E;
        font-family: "Kontora Medium";
        outline: none;
    }
    #root>div {position: relative;}

    a {
        text-decoration: none;
    }

    p {
        font-weight: 500;
        font-size: 14px;
        line-height: 140%;
        color: #2E2E2E;
        margin: 0;
    }

    h1, h2 {
        font-size: 28px;
        font-family: "Kontora Bold";
        color: #072847;
    }
    @media only screen and (min-width: 1440px) {
        h2 {
            font-size: 36px;
        }
    }
`;

export const Logo = styled.div<{ isWhite: boolean }>`
    width: 156px;
    height: 55px;
    background: url(${LogoIcon}) 0 0 no-repeat;
    background-size: contain;
`;

const Button = styled.a.attrs((props) => ({
    href: props.href || '#contact-us',
}))<{
    disabled?: boolean;
}>`
    display: flex;
    justify-content: center;
    align-items: center;
    min-width: 220px;
    max-width: 310px;
    height: 50px;
    color: #072847;
    border-radius: 10px;
    border: none;
    font-weight: 600;
    font-size: 18px;
    cursor: pointer;
    transition: background-color 0.3s;
    font-family: 'Kontora Regular';
`;

export const YellowButton = styled(Button)<{ isPending?: boolean }>`
    background-color: ${(props) => (props.disabled ? '#DFDFDF' : '#fdbc52')};
    color: ${(props) => (props.disabled ? '#9F9F9F' : '#072847')};

    &:hover {
        background-color: #fdb53f;
    }
`;

export const GhostButton = styled(Button)`
    background-color: rgba(0, 0, 0, 0);
    border: 1px solid #072847;
    color: #072847;

    &:hover {
        background-color: #072847;
        color: #fff;
    }
`;
