import React from 'react';
import { GhostButton } from '../ui';
import { WhoWeAreContainer } from './styled';

export const WhoWeAre = () => {
    return (
        <WhoWeAreContainer id="about">
            <h2>Who we are?</h2>
            <img src={require('../../assets/imgs/who-we-are.svg')} alt="" />
            <p>
            NeaMob is offering an all-round online team who work together to create fully customizable programs that
                can provide measurable results for our clients. Our team consists of an experienced and agile team of
                digital talent, who are all at the disposal of the client. Anything from developers and analysts to
                growth specialist & creative consultants who are all at the disposal of the client.
            </p>
            <GhostButton>Contact Us</GhostButton>
        </WhoWeAreContainer>
    );
};
