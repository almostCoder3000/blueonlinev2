import * as React from 'react';
import { MainBlockContainer, Title } from './styled';
import { GhostButton, YellowButton } from '../ui';
import MainIllustration from '../../assets/imgs/main-block-img.svg';

export default function MainBlock() {
    return (
        <MainBlockContainer>
            <Title>
                NeaMob<br></br>
                <span>Is All You Need for Online Growth</span>
            </Title>
            <p>
                NeaMob is a turnkey company that covers every aspect of online growth that is needed to be done for the
                successful business
            </p>
            <img src={MainIllustration} alt={''} />
            <div className="buttons">
                <YellowButton>Contact Us</YellowButton>
                <GhostButton>Learn more</GhostButton>
            </div>
            <div className="description">
                <div className="description-first">
                NeaMob will take care of everything you need on the online side. That way the process will be quick
                    and simple
                </div>
                <div className="description-second">
                NeaMob has 10-year experience with all marketing platforms making it easy to build a simple and
                    effective plan for any client type
                </div>
            </div>
        </MainBlockContainer>
    );
}
