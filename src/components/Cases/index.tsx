import React from 'react';
import { YellowButton } from '../ui';
import { CaseItem } from './CaseItem';
import { CASE_ITEMS, CaseType } from './data';
import { CaseList, CasesContainer, CTABlock, DescriptionCTA } from './styled';

export const Cases = () => {
    return (
        <CasesContainer id="cases">
            <h2>Cases</h2>
            <CaseList>
                {CASE_ITEMS.map((caseItem: CaseType, ind: number) => (
                    <CaseItem {...caseItem} key={ind} />
                ))}
            </CaseList>

            <CTABlock>
                <DescriptionCTA>
                    <h3>Do you like what you see?</h3>
                    <p>
                        Leave you contact details and our team will reach our quickly to discuss your online future. Our
                        consultations are free and discreet. Don’t leave without receiving new insightful information
                    </p>
                </DescriptionCTA>
                <YellowButton>Get your solution</YellowButton>
            </CTABlock>
        </CasesContainer>
    );
};
