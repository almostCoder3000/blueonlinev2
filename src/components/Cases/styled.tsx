import styled from 'styled-components';
import { YellowButton } from '../ui';

export const CasesContainer = styled.div`
    padding: 40px 25px 30px;
    h2 {
        margin: 0 0 20px;
    }
    @media only screen and (min-width: 768px) {
        padding: 40px 45px 68px;
    }
    @media only screen and (min-width: 1440px) {
        margin: 0 calc(50% - 600px);
    }
`;

export const CaseList = styled.div`
    display: flex;
    flex-direction: column;

    @media only screen and (min-width: 700px) {
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: space-between;
    }
    @media only screen and (min-width: 1024px) {
        margin: 0 -15px;
    }
`;

export const Title = styled.span`
    font-weight: 900;
    font-size: 30px;
    line-height: 150%;
    text-align: center;
    color: #072847;
    margin: 0;
    font-family: 'Kontora ExtraBlack';
`;

export const Description = styled.span`
    text-align: center;
    font-size: 14px;
    color: #2e2e2e;
    margin-bottom: 20px;
`;

export const CaseBox = styled.div`
    display: flex;
    flex-direction: column;
    &:not(:last-child) {
        margin-bottom: 30px;
    }

    img {
        height: 176px;
    }

    @media only screen and (min-width: 768px) {
        max-width: 213px;

        ${Title} {
            font-size: 24px;
        }
        ${Description} {
            font-size: 10px;
        }
        img {
            width: 100%;
            height: auto;
            max-height: 200px;
        }
    }
    @media only screen and (min-width: 1024px) {
        width: 33.33%;
        max-width: 350px;

        padding: 0px 15px;

        ${Title} {
            font-size: 38px;
        }
        ${Description} {
            font-size: 13px;
        }
    }
`;

export const CTABlock = styled.div`
    display: none;

    @media only screen and (min-width: 768px) {
        background: #f9fbfc;
        border-radius: 20px;
        padding: 28px 25px;
        display: flex;
        flex-direction: row;

        ${YellowButton} {
            margin: 25px 0 0 50px;
        }
    }

    @media only screen and (min-width: 1024px) {
        justify-content: space-between;
        padding: 28px 40px;
        ${YellowButton} {
            margin: auto 0;
        }
    }
`;

export const DescriptionCTA = styled.div`
    display: flex;
    flex-direction: column;
    padding-left: 93px;
    position: relative;

    h3 {
        margin: 0 0 15px;
    }
    p {
        font-size: 12px;
    }

    &:before {
        content: '';
        display: block;
        width: 73px;
        height: 75px;
        position: absolute;
        top: 0;
        left: 0;
        background: url(${require('../../assets/imgs/do-you-like-what-you-see.svg')}) 0 0 no-repeat;
    }

    @media only screen and (min-width: 1024px) {
        font-size: 14px;
        max-width: 632px;
    }
`;
