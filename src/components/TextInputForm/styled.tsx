import styled, { css } from 'styled-components';

export const Label = styled.label`
    font-weight: 600;
    font-size: 14px;
    line-height: 140%;
    color: #222222;
    margin: 0 0 5px 15px;
`;

const BaseTextInputMixin = css`
    background: #f9fbfc;
    box-sizing: border-box;
    border-radius: 10px;
    font-size: 16px;
    padding-left: 20px;

    &::placeholder {
        font-weight: 500;
        font-size: 16px;
        line-height: 140%;
        color: #a3becc;
    }
`;

export const InputText = styled.input<{ error?: string }>`
    border: 1px solid ${(props) => (props.error ? '#fdaf26' : '#e1edf2')};
    height: 50px;
    ${BaseTextInputMixin}
`;

export const Textarea = styled.textarea<{ error?: string }>`
    height: 188px;
    padding-top: 15px;
    border: 1px solid ${(props) => (props.error ? '#fdaf26' : '#e1edf2')};
    ${BaseTextInputMixin}
`;

export const ErrorMessage = styled.div`
    color: #fdaf26;
    font-size: 13px;
    position: absolute;
    bottom: -20px;
    left: 20px;
`;

export const InputWrapper = styled.div`
    display: flex;
    flex-direction: column;
    position: relative;
    margin-bottom: 25px;
    width: 100%;
`;
