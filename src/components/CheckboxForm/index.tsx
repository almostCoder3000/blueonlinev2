import React from 'react';
import { CheckboxWrapper, ErrorMessage } from './styled';

interface Props extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    label: string;
    error?: string;
}

export const CheckboxForm = React.forwardRef((props: Props, ref) => {
    return (
        <CheckboxWrapper>
            <input {...(props as any)} type="checkbox" ref={ref} />
            <div className="checkmark"></div>
            <span>{props.label}</span>
            <ErrorMessage>{props.error}</ErrorMessage>
        </CheckboxWrapper>
    );
});
