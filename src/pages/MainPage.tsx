import * as React from 'react';
import Header from '../components/Header';
import { GlobalStyle } from '../components/ui';
import MainBlock from '../components/MainBlock';
import OurServices from '../components/OurServices';
import { Cases } from '../components/Cases';
import { WhoWeAre } from '../components/WhoWeAre';
import { ContactUs } from '../components/ContactUs';
import { Footer } from '../components/Footer';
import { useEffect } from 'react';
import ReactGA from 'react-ga';

export default function MainPage() {
    useEffect(() => {
        ReactGA.initialize('UA-178462528-1');
        ReactGA.pageview('/');
    }, []);
    return (
        <React.Fragment>
            <GlobalStyle />
            <Header />
            <MainBlock />
            <OurServices />
            <Cases />
            <WhoWeAre />
            <ContactUs />
            <Footer />
        </React.Fragment>
    );
}
